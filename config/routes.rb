Rails.application.routes.draw do

  resources :shows
	root 'actors#index'
	
	resources :actors
	resources :movies
	
end

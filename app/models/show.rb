class Show < ActiveRecord::Base
	
	has_many :televisions
	has_many :actors, through: :televisions
	
	accepts_nested_attributes_for :actors,
																reject_if: proc { |attributes| attributes['name'].blank? },
																allow_destroy: true
	
end

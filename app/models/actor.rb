class Actor < ActiveRecord::Base
	
	belongs_to :collection
	has_many :studios
	has_many :televisions
	has_many :movies, through: :studios
	has_many :shows, through: :televisions
	accepts_nested_attributes_for :movies
	accepts_nested_attributes_for :shows
	
	validates :name,
						uniqueness: true
						
end

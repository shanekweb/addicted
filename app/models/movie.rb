class Movie < ActiveRecord::Base

	has_many :studios
	has_many :actors, through: :studios
	accepts_nested_attributes_for :actors,
																reject_if: proc { |attributes| attributes['name'].blank? },
																allow_destroy: true
	
end

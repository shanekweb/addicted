class ActorsController < ApplicationController
	
  def index
		@actors = Actor.all.order("name")
  end
  
  def show
	  @actor = Actor.find(params[:id])
	  @movies = @actor.movies
	 end
  
  def new
	  @actor = Actor.new
	end
	
	def create
		@actor = Actor.new(actor_params)
		@actor.save
		redirect_to @actor
	end
	
	def edit
		@actor = Actor.find(params[:id])
		@movies = @actor.movies
	end
	
	def update
		@actor = Actor.find(params[:id])
		@actor.update(actor_params)
		if @actor.save
			redirect_to @actor
		else
			render :edit
		end
	end
	
	def destroy
		@actor = Actor.find(params[:id]).destroy
		redirect_to root_path
	end
	
	private
	
	def actor_params
		params.require(:actor).permit(:name, :image, :imdb)	
	end

end

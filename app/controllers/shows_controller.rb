class ShowsController < ApplicationController
	
	def index
		@shows = Show.all.order('name')
	end
	
	def show
		@show = Show.find(params[:id])
		@actors = @show.actors
	end
	
	def edit
	  @show = Show.find(params[:id])
	  @actors = @show.actors
	  @show.actors.build
  end

  def update
	  @show = Show.find(params[:id])
	  @show.update(show_params)
	  if @show.save
			redirect_to @show
		else
		  render :edit
		end
  end
	
	def new
		@show = Show.new
		@show.actors.build
		@all_actors = Actor.all.order('name')
	end
	
	def create
		@show = Show.new(show_params)
		params[:actors][:id].each do |actor|
			if !actor.empty?
				@show.televisions.build(:actor_id => actor)	
			end
		end

		if @show.save
			redirect_to @show
		else
			render :new
		end
	end
	
	def destroy
		@show = Show.find(params[:id]).destroy
		redirect_to movie_path
	end
	
	private
	
	def show_params
		params.require(:show).permit(:name, :description, :rating, :image, :genre, actors_attributes: [:id, :name, :_destroy])
	end

end

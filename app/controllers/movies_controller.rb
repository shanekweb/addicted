class MoviesController < ApplicationController
	
  def index
	  @movies = Movie.all.order("title")
  end

  def show
	  @movie = Movie.find(params[:id])
	  @actors = @movie.actors
  end
  
  def new
	  @movie = Movie.new
	  @movie.actors.build
	  @all_actors = Actor.all.order('name')
  end

  def create
	  @movie = Movie.new(movie_params)
	  params[:actors][:id].each do |actor|
			if !actor.empty?
				@movie.studios.build(:actor_id => actor)	
			end
		end
	  @movie.save
	  redirect_to @movie
  end

  def edit
	  @movie = Movie.find(params[:id])
	  @actors = @movie.actors
	  @movie.actors.build
  end

  def update
	  @movie = Movie.find(params[:id])
	  @movie.update(movie_params)
	  if @movie.save
			redirect_to @movie
		else
		  render :edit
		end
  end

  def destroy
	  @movie = Movie.find(params[:id]).destroy
	  redirect_to root_path
  end
  
  private
  
  def movie_params
		params.require(:movie).permit(:title, :image, :description, :rating, :genre, :year, actors_attributes: [:id, :name, :_destroy])  
	end
  
end

class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :title
      t.text :description
      t.string :genre
      t.string :year
      t.decimal :rating
      t.integer :actor_id
      t.string :image

      t.timestamps null: false
    end
  end
end

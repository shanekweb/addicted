class RemoveIdFromActorsAndMovies < ActiveRecord::Migration
  def change
	  remove_column :actors, :movie_id
	  remove_column :movies, :actor_id
  end
end
